/*
 * Copyright (c) 2017,2018 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef __GST_QTIBUF_H__
#define __GST_QTIBUF_H__

#include <gst/gst.h>
#include <gst/video/video.h>

G_BEGIN_DECLS

GstAllocator * gst_qtibuf_allocator_new (guint * alloc_flags);

/* it's only valid when this utils is configured as ion.
 * @data: the va address
 * @fd: the fd represents the memory
 * @size: the size of the memory
 * @offset: the offset of the memory
 * @maxsize: the maxsize of the memory
 * @user_data: user_data for notification
 * @notify: notify callback for memory free
 */
GstMemory * gst_qtibuf_memory_new (GstMemoryFlags flags, gpointer data,
    gint fd, gsize size, gsize offset, gsize maxsize, gpointer user_data,
    GDestroyNotify notify);

gboolean gst_is_qti_allocator (GstAllocator * alloc);
gboolean gst_is_qti_memory (GstMemory * mem);
gboolean gst_qtibuf_memory_get_infos (GstMemory *mem, gint *fd, gsize *size,
    gsize *offset, gsize *maxsize, gpointer *data, gint *meta_fd);

/** the following apis were designed for the component which wants
 * to use this kind of allocator for buffer allocation rather than
 * get a buffer from buffer pool, e.g. gst_qtibuf_allocator_alloc,
 * gst_buffer_new_allocate, when the backend of utils was configured
 * as gbm. So the apis only have impact on gbm backend, that is only
 * video buffer allocation is supported by gbm-backend allocator.
 *
 * If component doesn't use it for buffer allocation, these can be
 * ignored.
 *
 * !!!!!! NOTE: 1, any component must follow below steps to use
 * the allocator, lock->config->allocate->unlock, to ensure
 * it works as expected, otherwise the behavior is undefined.
 * 2, allocator with gbm backend cannot work with other pool,
 * e.g. videopool, properly.
 *
 * example:
 *
 * gsize size;
 * GstVideoAlignment align;
 * GstVideoInfo info;
 * gpointer owner = (gpointer) component;
 * GstAllocator *alloc = gst_qti_allocator_new (NULL);
 * GstClockTime timeout = 2 * GST_SECOND;
 *
 * gst_video_info_from_caps (&info, caps);
 * size = gst_qtibuf_allocator_query_layout (alloc,
 * GST_VIDEO_FORMAT_NV12, 1280, 720, FALSE, &align);
 *
 * if (!size) {
 *   GST_WARNING ("query failed");
 *   return ;
 * }
 *
 * gst_qtibuf_allocator_lock (alloc, owner,
 *    GST_VIDEO_FORMAT_NV12, 1280, 720, FALSE);
 * gst_buffer_new_allocate (alloc, size, NULL);
 * gst_qtibuf_allocator_unlock (alloc, owner);
 *
 * gst_video_info_align (&info, &align);
 *
 */
gsize gst_qtibuf_allocator_query_layout (GstVideoFormat format,
    guint width, guint height, gboolean ubwc, GstVideoAlignment *align);
void gst_qtibuf_allocator_lock (GstAllocator *alloc, gpointer owner,
    GstVideoFormat format, guint width, guint height, gboolean ubwc);
/* only the owner of the lock can unlock the allocator */
void gst_qtibuf_allocator_unlock (GstAllocator *alloc, gpointer owner);
/** only flags of params was supported */
GstMemory * gst_qtibuf_allocator_alloc (GstAllocator *alloc,
    gsize size, GstAllocationParams *params);


G_END_DECLS

#endif /* __GST_QTIBUF_H__ */
